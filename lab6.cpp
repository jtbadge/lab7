/*
Jake Badger
CPSC 1021, Section 3, F20
jtbadge@g.clemson.edu
TA: Elliot McMillan
Lab 6

The purpose of this lab was to practice and understand the use of different
C++ functions and language coding that is different from C (cout, cin etc). 
This lab was done to take a typedefed struct, create separate arrays of structs
from the struct, and randomly generate different outputs. The user will be 
asked to input 10 different employees and then call a random generator using a
seed via srand. The program will then randomly shuffle the array, and then 
print back to the user the first 5 elements of that array sorted in 
alphabetical order by last name. The function uses random_shuffle() to carry
out the random sort, sort() to sort the first 5 elements, and then a bool 
function to return true when the left hand side of the array is smaller than
the right hand side (i.e. the last name is higher alphabetically).
*/

//prototypes needed for cout, random generators and sort functions, and 
//taking input from the user. This is also needed for the for loops and 
//auto below
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

//to make calling cout and other functions easier
using namespace std;

//typedefed struct Employee containing initial parameters for the array of 
//structures
typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

//function prototypes for the sort function and random shuffle, respectively
bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


//main function
int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));


  /*Create an array of 10 employees and fill information from standard input
  with prompt messages*/

  //initialize array of structures of size 10, and assigning function pointers
  //to point to the front and one past the back of the array. This is to give
  //parameters to the sort and shuffle functions
  employee employees[10];
  employee *frontPtr;
  employee *endPtr;
  int i=0;

  //for loop to initialize the array and structure via user input
  for(i=0; i<10; i++) {
	cout << "Please enter employee " << i+1 << "'s first name: ";
	cin >> employees[i].firstName; 
	cout << "Please enter the emplyee's last name: ";
	cin >> employees[i].lastName;
	cout << "Please insert employee's year of birth: ";
	cin >> employees[i].birthYear;
	cout << "Please insert employee's hourly wage: ";
	cin >> employees[i].hourlyWage;
  }

  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

  //assigning the front and back pointer to point to the front and back of the
  //array
  frontPtr = &employees[0];
  endPtr = &employees[10];

  //randomly shuffling the array structures
  random_shuffle(frontPtr, endPtr, myrandom);

  /*Build a smaller array of 5 employees from the first five cards of the array
   created *above*/
  
  employee employee2[5] = {employees[0], employees[1], employees[2],
    employees[3], employees[4]};

    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/

  //assigning the two pointers to point to the new array so the correct array
  //gets sorted in the output
  frontPtr = &employee2[0];
  endPtr = &employee2[5];

  //sorting the function
  sort(frontPtr, endPtr, name_order);

    /*Now print the array below */
  
  //range based for loop to print out the values for each parameter of the
  //struct to the user. The setw function gives the total width of the names
  //set aside, and right aligns the values to the right side of the width
  //fixed, showpoint, and setprecision are to allow for the double value
  //of the wage to be printed to 2 decimal places and to show the decimal
  for(auto x: employee2) {
    cout << setw(30) << right << x.lastName + ", " + x.firstName << "\n";
    cout << setw(30) << right << x.birthYear << "\n";
    cout << setw(30) << right << fixed << showpoint << setprecision(2)
	 << x.hourlyWage << "\n";
  }


  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
  // IMPLEMENT
  //returns true if the value on the left is lower than the right
  //basically saying the left is higher alphabetically
  return lhs.lastName < rhs.lastName;
  
} 
